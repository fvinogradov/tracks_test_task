# Requirements:
- PostgreSQL 9.6+
- Redis
- Clickhouse

# Configurations

## .env
Replace .env.example with .env file and set all variables in it

## Postgre Configuration:
Run the following queries in Postgre console:
```
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE trackers (
    id bigserial PRIMARY KEY,
    uuid uuid DEFAULT uuid_generate_v4(),
    value character varying(32) NOT NULL
);

CREATE FUNCTION notify_trigger() RETURNS trigger AS $trigger$
DECLARE
    rec RECORD;
    payload TEXT;
    column_name TEXT;
    column_value TEXT;
    payload_items TEXT[];
BEGIN
    -- Set record row depending on operation
    CASE TG_OP
        WHEN 'INSERT', 'UPDATE' THEN
            rec := NEW;
        WHEN 'DELETE' THEN
            rec := OLD;
        ELSE
            RAISE EXCEPTION 'Unknown TG_OP: "%". Should not occur!', TG_OP;
        END CASE;

    -- Get required fields
    FOREACH column_name IN ARRAY TG_ARGV LOOP
            EXECUTE format('SELECT $1.%I::TEXT', column_name)
                INTO column_value
                USING rec;
            payload_items := array_append(payload_items, '"' || replace(column_name, '"', '\"') || '":"' || replace(column_value, '"', '\"') || '"');
        END LOOP;

    -- Build the payload
    payload := ''
                   || '{'
                   || '"timestamp":"' || CURRENT_TIMESTAMP                    || '",'
                   || '"operation":"' || TG_OP                                || '",'
                   || '"schema":"'    || TG_TABLE_SCHEMA                      || '",'
                   || '"table":"'     || TG_TABLE_NAME                        || '",'
                   || '"data":{'      || array_to_string(payload_items, ',')  || '}'
        || '}';

    -- Notify the channel
    PERFORM pg_notify('tracks_notify_channel', payload);

    RETURN rec;
END;
$trigger$ LANGUAGE plpgsql;

CREATE TRIGGER trackers_notify AFTER INSERT OR UPDATE ON trackers
    FOR EACH ROW EXECUTE PROCEDURE notify_trigger(
        'uuid',
        'value'
    );

```

## ClickHouse Configuration:
Run the following queries in clickhouse-client console:
```sql
CREATE TABLE tracking_events
(
    date       Date,
    date_time  DateTime,
    event_id   String,
    tracker_id String,
    ip         String,
    user_id    String,
    user_agent String,
    url        String,
    value      String
)  ENGINE = MergeTree PARTITION BY toYYYYMM(date) ORDER BY (tracker_id) SETTINGS index_granularity = 8192;
```

## Testing Data
If you want to use Artillery (usage described below), you will need to get generated tracker-id's from Postgre
and insert them into `test/load/trackers.csv`

# Testing:

```bash
npm i -g artillery
npm start
artillery run test/load/artillery.yml  
```