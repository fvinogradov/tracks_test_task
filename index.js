const http = require('http');
const { promisify } = require("util");
const dotenv = require('dotenv-safe');
const { initPG, initNotifyListener } = require('./src/connectors/pg');
const { initRedis } = require('./src/connectors/redis');
const { initClickhouse } = require('./src/connectors/clickhouse');
const TracksController = require('./src/controllers/tracks');
const TrackersController = require('./src/controllers/trackers');
const StatsController = require('./src/controllers/stats');
const cookie = require('cookie');

const PORT = process.env.PORT || 3000;

let pgClient;
let redisClient;
let clickhouseClient;
let getAsync;
let tracksController;
let statsController;
let trackersController;

(async () => {
    const env = dotenv.config().parsed;
    pgClient = await initPG(env.PG_URL);

    redisClient = await initRedis();
    getAsync = promisify(redisClient.get).bind(redisClient);
    clickhouseClient = await initClickhouse(env.CLICKHOUSE_HOST, env.CLICKHOUSE_PORT);

    tracksController = new TracksController(redisClient, clickhouseClient);
    statsController = new StatsController(clickhouseClient);
    trackersController = new TrackersController(pgClient, redisClient);
    await trackersController.putAllTrackersToCache();
})();

const server = http.createServer(async (req, res) => {
    const url = new URL(req.url, `http://${req.headers.host}`);
    if (url.pathname.startsWith('/track')) {
        const id = url.searchParams.get('id');
        const trackerValue = await getAsync(id);
        if (trackerValue) {
            const eventData = statsController.prepareEventData(req, id, trackerValue, url);
            tracksController.setTrackingEvents(eventData);
            res.setHeader('Set-Cookie', cookie.serialize('user_id', eventData.user_id));
            res.statusCode = 204;
        } else {
            res.statusCode = 404;
            res.write("TrackId Not Found");
        }
    } else if (url.pathname.startsWith('/stats')) {
        const trackerId = url.searchParams.get('tracker_id');
        const from = url.searchParams.get('from');
        const to = url.searchParams.get('to');
        const count = await statsController.getStats(trackerId, from, to);
        res.write(`Count: ${count}`);
    } else {
        res.statusCode = 404;
        res.write("Not Found");
    }
    res.end();
});

server.listen(PORT);
