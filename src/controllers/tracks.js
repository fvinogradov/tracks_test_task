let trackingEvents = [];
const insertIntervalMilliseconds = 2500;

class TracksController {
    constructor(redisClient, clickhouseClient) {
        this.redis = redisClient;
        this.clickhouse = clickhouseClient;
        this.bulkInsert = this.bulkInsert.bind(this);
        setInterval(this.bulkInsert, insertIntervalMilliseconds);
    }

    setTrackingEvents(payload) {
        trackingEvents.push(payload);
    }

    bulkInsert() {
        if (trackingEvents.length > 0) {
            const stream = this.clickhouse.query("INSERT INTO tracking_events", { format: 'JSONEachRow' });
            for (const event of trackingEvents) {
                stream.write(event);
            }
            stream.end();
            trackingEvents = [];
        }
    }


}

module.exports = TracksController;