const cookie = require('cookie');
const uuid = require('uuid').v4;

const getUserId = (req) => {
    const cookies = cookie.parse(req.headers.cookie || '');
    return cookies['user_id'] ? cookies['user_id'] : uuid();
}

module.exports = {
    getUserId
}
