const { initNotifyListener } = require('../connectors/pg');


class TrackersController {
    constructor(pgClient, redisClient) {
        this.pg = pgClient;
        this.redis = redisClient;
        this.subscribeForTrackersUpdateInPostgre();
    }

    async putAllTrackersToCache() {
        // TODO: replace with pg-cursor if a huge amount of trackers is expected
        const pgResults = await this.pg.query('select * from trackers');
        const batch = this.redis.batch();
        for (const row of pgResults.rows) {
            batch.set(row['uuid'], row['value']);
        }
        batch.exec(err => {
            if (err) {
                console.log('Loading trackers to Redis failed with error:', err);
                process.exit(1);
            }
        });
    }

    subscribeForTrackersUpdateInPostgre() {
        initNotifyListener(this.pg, msg => {
            msg = JSON.parse(msg);
            this.redis.set(msg.data.uuid, msg.data.value);
        });
    }
}

module.exports = TrackersController;