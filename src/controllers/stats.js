const uuid = require('uuid').v4;
const {getUserId} = require('./users');
const {format, addMinutes} = require('date-fns');

class StatsController {
    constructor(clickhouseClient) {
        this.clickhouse = clickhouseClient;
    }

    formatDate(date, dateFormat) {
        return format(addMinutes(date, date.getTimezoneOffset()), dateFormat);
    }

    async getStats(trackerId, fromDate, toDate) {
        let where = `tracker_id = '${trackerId}'`;
        if (fromDate) where += ` AND date >= '${fromDate}'`;
        if (toDate) where += ` AND date <= '${toDate}'`;
        const {data} = await this.clickhouse.querying(`SELECT count() as count
                                                  from tracking_events
                                                  WHERE ${where}`)
        return data[0][0];
    }

    prepareEventData(req, trackedId, trackerValue, url) {
        const eventDate = new Date();
        return {
            date: this.formatDate(eventDate, 'yyyy-MM-dd'),
            date_time: this.formatDate(eventDate, 'yyyy-MM-dd HH:mm:ss'),
            event_id: uuid(),
            tracker_id: trackedId,
            ip: (req.headers['x-forwarded-for'] || '').split(',').pop().trim() ||
                req.socket.remoteAddress,
            user_id: getUserId(req),
            user_agent: req.headers['user-agent'],
            url: url.href,
            value: trackerValue,
        };
    };
}

module.exports = StatsController;