const Clickhouse = require('@apla/clickhouse');

const initClickhouse = async (host, port) => {
    try {
        const client = new Clickhouse({ host, port });
        await client.querying("SELECT 1");
        console.log('Connection to Clickhouse established');
        return client;
    } catch (err) {
        console.log(`Connection to Clickhouse failed with error: ${err}`)
        process.exit(1);
    }
};

module.exports = {
    initClickhouse
}