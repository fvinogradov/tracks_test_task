const Postgre = require('pg');
const notifyChannel = 'tracks_notify_channel'

const initPG = async (connectionString) => {
    const client = new Postgre.Client(connectionString);
    await client.connect();
    console.log('Connection to Postgre established');
    return client;
}

const initNotifyListener = (client, cb) => {
    client.on('notification', (msg) => {
        if (msg.channel == notifyChannel) {
            cb(msg.payload);
        }
    });
    client.query(`LISTEN ${notifyChannel}`);
}

module.exports = {
    initPG,
    initNotifyListener
}