const Redis = require('redis');

const initRedis = async () => {
    const client = Redis.createClient();

    client.on("error", function (error) {
        console.error(error);
    });
    console.log('Connection to Redis established');
    return client;
}

module.exports = {
    initRedis
}